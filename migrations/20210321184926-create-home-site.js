'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('HomeSites', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Logo: {
        type: Sequelize.STRING
      },
      carrouselTitle: {
        type: Sequelize.STRING
      },
      carrouselImages: {
        type: Sequelize.JSON
      },
      carrouselGaleria: {
        type: Sequelize.JSON
      },
      video: {
        type: Sequelize.STRING
      },
      congressDate: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('HomeSites');
  }
};