'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Activities', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE
      },
      name: {
        type: Sequelize.STRING
      },
      schedule: {
        type: Sequelize.STRING
      },
      accessLimit: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING
      },
      urlConference: {
        type: Sequelize.STRING
      },
      category: {
        type: Sequelize.ENUM(['retail', 'activity', 'conference'])
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Activities');
  }
};