const express = require('express');
const router = express.Router();
const { getAllHomeSiteInfo, addOrUpdateHomeSite } = require('../controllers/homesite');

router.get('/', getAllHomeSiteInfo);
router.post('/', addOrUpdateHomeSite);

module.exports = router;