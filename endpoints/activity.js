const express = require('express');
const router = express.Router();
const { getAllActivities, addActivity, updateActivity, removeActivity, registerWebUserToActivity } = require('../controllers/activity');

router.get('/findAll', getAllActivities);
router.post('/', addActivity);
router.put('/:activityId', updateActivity);
router.delete('/:activityId', removeActivity);
router.post('/registerUserToActivity', registerWebUserToActivity);

module.exports = router;