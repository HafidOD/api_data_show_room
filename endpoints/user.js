const express = require("express");
const router = express.Router();
const { SignUp } = require("../controllers/userweb");
const { passportUserWebMiddleware } = require("../config/localStrategyWeb");
const jwt = require("jsonwebtoken");

router.post("/register", SignUp);
router.post(
  "/login",
  async (req, res, next) => {
    passportUserWebMiddleware.authenticate("local", async (err, user, info) => {
      try {
        if ((err || info) && !user) {
          throw info.message;
        }
        const body = { _id: user._id, email: user.email, firstName:user.firstName, lastName:user.lastName, role:'webuser' };
        //const token = jwt.sign({ user: body }, process.env.JWT_SECRET);
        res.locals.response = body;
      } catch (error) {
        res.locals.error = { message: error };
      } finally {
        next();
      }
    })(req, res, next);
  },
  (req, res) => {
    if (res.locals.error != null) {
      return res.status(500).json(res.locals.error);
    }
    return res.status(200).json(res.locals.response);
  }
);

module.exports = router;
