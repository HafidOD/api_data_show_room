const express = require('express');
const router = express.Router();
const { getAllPartners, addPartner, updatePartner, removePartner } = require('../controllers/partners');

router.get('/findAll', getAllPartners);
router.post('/', addPartner);
router.put('/:partnerId', updatePartner);
router.delete('/:partnerId', removePartner);

module.exports = router;