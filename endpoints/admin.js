const express = require('express');
const router = express.Router();
const { SignUpAdmin } = require('../controllers/adminuser');
const { passportAdminMiddleware } = require('../config/localStrategy');
const jwt = require('jsonwebtoken');


router.post('/register', SignUpAdmin);
router.post('/login', async (req, res, next) => {
  passportAdminMiddleware.authenticate('local', async (err, user, info) => {
    try {
      if ((err || info) && !user) {
        throw info.message
      }
      const body = { _id: user._id, email: user.email, role:'admin' };
      //const token = jwt.sign({ user: body }, process.env.JWT_SECRET);
      res.locals.response = body;
    } catch (error) {
      res.locals.error = {message: error};
    } finally {
      next()
    }
  })(req, res, next);
},(req,res)=>{
  if(res.locals.error != null){
    return res.status(500).json(res.locals.error);
  }
  return  res.status(200).json(res.locals.response);
});


module.exports = router;