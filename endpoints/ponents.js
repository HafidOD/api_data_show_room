const express = require('express');
const router = express.Router();
const { getAllPonents, addPonent, updatePonent, removePonent } = require('../controllers/ponents');

router.get('/findAll', getAllPonents);
router.post('/', addPonent);
router.put('/:ponentId', updatePonent);
router.delete('/:ponentId', removePonent);

module.exports = router;