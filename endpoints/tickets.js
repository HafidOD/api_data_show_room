const express = require('express');
const router = express.Router();
const {
    getAllTypeOfTickets,
    addTypeOfTickets,
    updateTypeOfTickets,
    removeTypeOfTickets
} = require('../controllers/tickets');

router.get('/findAll', getAllTypeOfTickets);
router.post('/', addTypeOfTickets);
router.put('/:typeOfTicketId', updateTypeOfTickets);
router.delete('/:typeOfTicketId', removeTypeOfTickets);

module.exports = router;