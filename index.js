require("dotenv").config();
const express = require("express");
const passport = require("passport");
const cors = require("cors");
const app = express();
app.use(cors());
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

const port = 7000;
const partnersRouter = require("./endpoints/partners");
const ponentsRouter = require("./endpoints/ponents");
const ticketsRouter = require("./endpoints/tickets");
const activityRouter = require("./endpoints/activity");
const adminAuthRouter = require("./endpoints/admin");
const userAuthRouter = require("./endpoints/user");
const homeSiteRouter = require("./endpoints/homesite");

app.get("/", (req, res) => {
  res.json({ status: 200, message: "Everything working!" });
});

app.use("/partners", partnersRouter);
app.use("/ponents", ponentsRouter);
app.use("/tickets", ticketsRouter);
app.use("/activities", activityRouter);
app.use("/homesite", homeSiteRouter);
app.use("/adminAuth", adminAuthRouter);
app.use("/userAuth", userAuthRouter);

app.listen(port, () => console.log("API is running..."));
