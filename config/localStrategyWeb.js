const passport = require("passport");
const bcrypt = require("bcrypt");
const nodeify = require("nodeify");
const LocalStrategy = require("passport-local").Strategy;
const { WebUser } = require("../models");

var user_cache = {};

async function validPassword(correctPassword, inputPassword) {
  const match = await bcrypt.compare(inputPassword, correctPassword);
  return match;
}

/*email,pass */
async function localStrategyFunction(email, password, done) {
  try {
    email = email.toLowerCase();
    const webUserObj = await WebUser.findOne({ where: { email: email } });
    if (webUserObj === null) {
      return done(null, false, { message: "Incorrect email." });
    }
    if (!(await validPassword(webUserObj.password, password))) {
      return done(null, false, { message: "Incorrect password." });
    }
    return done(null, webUserObj.dataValues, {
      message: "Logged in Successfully",
    });
  } catch (error) {
    return done(error);
  }
}
passport.serializeUser(function (user, done) {
  let { id } = user;
  user_cache[id] = user;
  done(null, id);
  // if you use Model.id as your idAttribute maybe you'd want
  // done(null, user.id);
});
passport.deserializeUser(function (id, done) {
  done(null, user_cache[id]);
});

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
    },
    (email, password, done) => {
      nodeify(localStrategyFunction(email, password, done), done, {
        spread: true,
      });
    }
  )
);

module.exports = {
  passportUserWebMiddleware: passport,
};
