'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class HomeSite extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  HomeSite.init({
    Logo: DataTypes.STRING,
    carrouselTitle: DataTypes.STRING,
    carrouselImages: DataTypes.JSON,
    carrouselGaleria: DataTypes.JSON,
    congressDate: DataTypes.DATE,
    video : DataTypes.STRING
  }, {
    sequelize,
    modelName: 'HomeSite',
  });
  return HomeSite;
};