'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WebUserActivityRelation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  WebUserActivityRelation.init({
    idEvent: DataTypes.INTEGER,
    idUserWeb: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'WebUserActivityRelation',
  });
  return WebUserActivityRelation;
};