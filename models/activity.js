'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Activity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Activity.init({
    date: DataTypes.DATE,
    name: DataTypes.STRING,
    schedule: DataTypes.STRING,
    accessLimit: DataTypes.INTEGER,
    category: DataTypes.ENUM(['retail', 'activity', 'conference']),
    description: DataTypes.STRING,
    urlConference: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Activity',
  });
  return Activity;
};