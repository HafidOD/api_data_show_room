const { AdminUser } = require('../models');
const bcrypt = require('bcrypt');
const saltRounds = Number(process.env.SALT_ROUNDS);
function encrypt(password){
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);
    return hash;
}
async function SignUpAdmin(req,res){
    let { body } = req;
    body.email = body.email.toLowerCase();
    body.password = await encrypt(body.password);
    const adminSearch = await AdminUser.findOne({where:{email:body.email}})
    if(adminSearch===null){
        const results = await AdminUser.create(body)
        res.json({ results })
        return
    }
    res.status(500).json({status:500, message:"User previously registered"});
}

module.exports = {
    SignUpAdmin
}