const { Ponents } = require('../models');
async function getAllPonents(req, res) {
  const results = await Ponents.findAll()
  res.json({ results })
}

async function addPonent(req, res) {
  const { body } = req;
  const results = await Ponents.create(body)
  res.json({ results })
}


async function updatePonent(req, res) {
  const { body, params } = req;
  const { ponentId } = params;
  const results = await Ponents.update(body, {
    where: {
      id: ponentId
    }
  })
  res.json({ results })
}

async function removePonent(req, res) {
  const { params } = req;
  const { ponentId } = params;
  const results = await Ponents.destroy({
    where: {
      id: ponentId
    }
  })
  res.json({ results })
}

module.exports = {
  getAllPonents,
  addPonent,
  updatePonent,
  removePonent
}