const { HomeSite } = require('../models');
async function getAllHomeSiteInfo(req, res) {
  const results = await HomeSite.findByPk(1)
  if(!results){
    res.status(404).json({message:"No home site register provided"})
  }
  res.status(200).json({ results })
}

async function addOrUpdateHomeSite(req, res) {
  const { body } = req;
  const results = await HomeSite.upsert({
    id:1,
    ...body
  })
  res.status(200).json({ results })
}

module.exports = {
  getAllHomeSiteInfo,
  addOrUpdateHomeSite,
}