const { Partners } = require('../models');
async function getAllPartners(req, res) {
  const results = await Partners.findAll()
  res.json({ results })
}

async function addPartner(req, res) {
  const { body } = req;
  const results = await Partners.create(body)
  res.json({ results })
}


async function updatePartner(req, res) {
  const { body, params } = req;
  const { partnerId } = params;
  const results = await Partners.update(body, {
    where: {
      id: partnerId
    }
  })
  res.json({ results })
}

async function removePartner(req, res) {
  const { params } = req;
  const { partnerId } = params;
  const results = await Partners.destroy({
    where: {
      id: partnerId
    }
  })
  res.json({ results })
}

module.exports = {
  getAllPartners,
  addPartner,
  updatePartner,
  removePartner
}