const { TypeOfTickets } = require('../models');
async function getAllTypeOfTickets(req, res) {
  const results = await TypeOfTickets.findAll()
  res.json({ results })
}

async function addTypeOfTickets(req, res) {
  const { body } = req;
  const results = await TypeOfTickets.create(body)
  res.json({ results })
}


async function updateTypeOfTickets(req, res) {
  const { body, params } = req;
  const { typeOfTicketId } = params;
  const results = await TypeOfTickets.update(body, {
    where: {
      id: typeOfTicketId
    }
  })
  res.json({ results })
}

async function removeTypeOfTickets(req, res) {
  const { params } = req;
  const { typeOfTicketId } = params;
  const results = await TypeOfTickets.destroy({
    where: {
      id: typeOfTicketId
    }
  })
  res.json({ results })
}

module.exports = {
  getAllTypeOfTickets,
  addTypeOfTickets,
  updateTypeOfTickets,
  removeTypeOfTickets
}