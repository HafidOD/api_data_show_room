const { WebUser } = require("../models");
const bcrypt = require("bcrypt");
const saltRounds = Number(process.env.SALT_ROUNDS);
function encrypt(password) {
  const salt = bcrypt.genSaltSync(saltRounds);
  const hash = bcrypt.hashSync(password, salt);
  return hash;
}
async function SignUp(req, res) {
  let { body } = req;
  body.email = body.email.toLowerCase();
  body.password = await encrypt(body.password);
  const userSearch = await WebUser.findOne({ where: { email: body.email } });
  if (userSearch === null) {
    const results = await WebUser.create(body);
    res.json({ results });
    return;
  }
  res.status(500).json({ status: 500, message: "User previously registered" });
}

module.exports = {
  SignUp,
};
