const { Activity, WebUser, WebUserActivityRelation } = require("../models");

/**
 * CRUD operations
 */
async function getAllActivities(req, res) {
  const results = await Activity.findAll({
    order: [
      ['date', 'ASC'],
      ['schedule', 'ASC']

    ],
  });
  res.json({ results });
}

async function addActivity(req, res) {
  const { body } = req;
  const results = await Activity.create(body);
  res.json({ results });
}

async function updateActivity(req, res) {
  const { body, params } = req;
  const { activityId } = params;
  const results = await Activity.update(body, {
    where: {
      id: activityId,
    },
  });
  res.json({ results });
}

async function removeActivity(req, res) {
  const { params } = req;
  const { activityId } = params;
  const results = await Activity.destroy({
    where: {
      id: activityId,
    },
  });
  res.json({ results });
}
/**
 * validations
 */
async function checkAvailability(ActivityObj) {
  const { id, accessLimit } = ActivityObj;
  const ocuppation = await WebUserActivityRelation.findAll({
    where: {
      idEvent: id,
    },
  });
  return ocuppation < accessLimit;
}
/**
 *  Relations
 */
async function registerWebUserToActivity(req, res) {
  const { body } = req;
  const { webUserId, activityId } = body;
  const webUserObj = await WebUser.findByPk(webUserId);
  if (webUserObj === null) {
    res.status(404).json({ status: 404, message: "User not found" });
    return;
  }
  const activityObj = await Activity.findByPk(activityId);
  if (activityObj === null) {
    res.status(404).json({ status: 404, message: "Activity not found" });
    return;
  }
  if (await checkAvailability(activityObj)) {
    const results = await WebUserActivityRelation.create({
      idEvent: activityId,
      idUserWeb: webUserId,
    });
    res.json({ status: 200, message: "Registered correctly", results });
    return;
  }
  res
    .status(500)
    .json({ status: 500, message: "No availability for the activity" });
}
module.exports = {
  getAllActivities,
  addActivity,
  updateActivity,
  removeActivity,
  registerWebUserToActivity,
};
